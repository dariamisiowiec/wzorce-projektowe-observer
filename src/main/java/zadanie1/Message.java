package zadanie1;

public class Message {
    private int idSender;
    private String textMessage;

    public int getIdSender() {
        return idSender;
    }

    public void setIdSender(int idSender) {
        this.idSender = idSender;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public Message(int idSender, String textMessage) {
        this.idSender = idSender;
        this.textMessage = textMessage;
    }
}
