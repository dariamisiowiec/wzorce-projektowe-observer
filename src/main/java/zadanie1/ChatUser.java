package zadanie1;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ChatUser implements Observer {
    private int id;
    private String nick;
    private List<String> messages;
    private boolean isAdmin;

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Message) {
            Message message = (Message)arg;
            if (message.getIdSender() == id)
                return;
            String textMessage = message.getTextMessage();

            //    int senderId = message.getIdSender();

            System.out.println(nick + " otrzymał wiadomość " + "o treści: " + textMessage);
        } else {
            System.out.println("Błąd");
        }
    }

    @Override
    public String toString() {
        return "ChatUser{" +
                "id=" + id +
                ", nick='" + nick + '\'' +
                ", messages=" + messages +
                ", isAdmin=" + isAdmin +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
