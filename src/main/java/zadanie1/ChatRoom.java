package zadanie1;

import java.util.*;

public class ChatRoom extends Observable {
    private int numberOfUsers;
    private Map<Integer, ChatUser> loggedUsers;
    private String roomName;
    private List<String> adminUsers = new LinkedList<>(Arrays.asList("admin", "administrator"));

    public Map<Integer, ChatUser> getLoggedUsers() {
        return loggedUsers;
    }

    public ChatRoom(String roomName) {
        this.roomName = roomName;
        numberOfUsers = 0;
    }

    public void userRegister(String nick){
        for (ChatUser user: loggedUsers.values()) {
            if (user.getNick().equals(nick)) {
                System.out.println("Użytkownik o tym nicku już istnieje");
                return;
            }
        }
        ChatUser newUser = new ChatUser();
        newUser.setNick(nick);
        if (adminUsers.contains(nick))
            newUser.setAdmin(true);
        else
            newUser.setAdmin(false);
        numberOfUsers++;
        newUser.setId(numberOfUsers);
        loggedUsers.put(numberOfUsers, newUser);
        addObserver(newUser);
    }

    public void sendMessage(int user, String message) {
        Message myMessage = new Message(user, message);
        setChanged();
        notifyObservers(myMessage);
    }

    public void kickUser(int idIsKicked, int idIsKicking) {
        if (adminUsers.contains(loggedUsers.get(idIsKicking).getNick())){
            loggedUsers.remove(idIsKicked);
        } else {
            System.out.println("Nie jesteś adminem");
        }
    }

}
